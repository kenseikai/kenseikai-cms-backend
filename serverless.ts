import type { AWS } from '@serverless/typescript';

import { nest } from '@functions/nest';

const serverlessConfiguration: AWS = {
  service: 'kenseikai-cms-backend',
  frameworkVersion: '2',
  custom: {
    deploymentBucket: {
      versioning: true,
    },
    customDomain: {
      domainName: 'api.archive.ut-shorinji.net',
      stage: 'production',
      basePath: '',
      createRoute53Record: true,
    },
    'serverless-layers': {
      dependenciesPath: './package.json',
    },
    'serverless-offline': {
      noPrependStageInUrl: true,
    },
    webpack: {
      webpackConfig: './webpack.config.js',
      includeModules: false,
      keepOutputDirectory: true,
    },
  },
  plugins: [
    'serverless-deployment-bucket',
    'serverless-domain-manager',
    'serverless-webpack',
    'serverless-layers',
    'serverless-offline',
  ],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    deploymentBucket: {
      name: 'kenseikai-serverless-${aws:accountId}',
    },
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      STAGE: '${sls:stage}',
    },
    lambdaHashingVersion: '20201221',
  },
  // import the function via paths
  functions: {
    nest,
    basicAuth: { handler: 'src/functions/auth/auth.handler' },
  },
  resources: {
    Resources: {
      GatewayResponse: {
        Type: 'AWS::ApiGateway::GatewayResponse',
        Properties: {
          ResponseParameters: {
            'gatewayresponse.header.WWW-Authenticate': "'Basic'",
            'gatewayresponse.header.Access-Control-Allow-Origin':
              "'https://archive.ut-shorinji.net'",
            'gatewayresponse.header.Access-Control-Allow-Headers': "'*'",
            'gatewayresponse.header.Access-Control-Allow-Methods':
              "'OPTIONS, GET'",
          },
          ResponseType: 'UNAUTHORIZED',
          RestApiId: { Ref: 'ApiGatewayRestApi' },
          StatusCode: '401',
        },
      },
      // GatewayResponse2: {
      //   Type: 'AWS::ApiGateway::GatewayResponse',
      //   Properties: {
      //     ResponseParameters: {
      //       'gatewayresponse.header.Access-Control-Allow-Origin': "'*'",
      //       'gatewayresponse.header.Access-Control-Allow-Headers': "'*'",
      //     },
      //     ResponseType: 'EXPIRED_TOKEN',
      //     RestApiId: { Ref: 'ApiGatewayRestApi' },
      //     StatusCode: '401',
      //   },
      // },
    },
  },
};

module.exports = serverlessConfiguration;
