import { Injectable } from '@nestjs/common';
import axios, { AxiosResponse } from 'axios';
import { VimeoFolder, VimeoResult } from './vimeo';
import { FolderType, VideoType } from './vimeo.dto';

@Injectable()
export class VimeoService {
  token: string;
  constructor() {
    this.token = 'bearer c614e9f1020f392577654a42768de34e';
  }

  async getFolders() {
    const result = (await axios.get('https://api.vimeo.com/me/projects', {
      headers: {
        Authorization: this.token,
      },
    })) as AxiosResponse<VimeoResult<VimeoFolder>>;
    return result.data.data.map((f) => {
      return {
        name: f.name,
        uri: f.uri,
        parent_uri: f.metadata.connections.parent_folder?.uri,
      };
    });
  }

  async getRootFolder() {
    // https://api.vimeo.com/me?fields=metadata.connections.folders_root
    // {
    //   "metadata": {
    //       "connections": {
    //           "folders_root": {
    //               "uri": "/users/142549013/folders/root",
    //               "options": [
    //                   "GET"
    //               ]
    //           }
    //       }
    //   }
    //  }
    const result = (await axios.get(
      'https://api.vimeo.com/me?fields=metadata.connections.folders_root',
      {
        headers: {
          Authorization: this.token,
        },
      },
    )) as AxiosResponse<any>;
    const root_folder_uri = result.data.metadata.connections.folders_root.uri;
    const list = await this.scanVimeoApi(root_folder_uri + '?per_page=100');
    return this.prettifyVimeoDataList(list);
  }

  async getFolder(folder_id?: string) {
    const list = await this.scanVimeoApi(
      `/users/142549013/projects/${folder_id}/items??per_page=100`,
    );
    return this.prettifyVimeoDataList(list);
  }

  async scanVimeoApi(uri: string) {
    const buffer = [];
    let next_uri = uri;
    while (next_uri) {
      console.log('vimeo api request', next_uri);
      const result = (await axios.get(`https://api.vimeo.com${next_uri}`, {
        headers: {
          Authorization: this.token,
        },
      })) as AxiosResponse<any>;
      Array.prototype.push.apply(buffer, result.data.data);
      console.log(
        'vimeo api X-RateLimit-Remaining',
        result.headers['x-ratelimit-remaining'],
      );
      next_uri = result.data.paging.next;
    }

    return buffer;
  }

  async prettifyVimeoDataList(list: any[]) {
    const buffer = [];
    for (const data of list) {
      if (data.type == 'video') {
        const video: VideoType = {
          created_time: data.video.created_time,
          modified_time: data.video.modified_time,
          release_time: data.video.release_time,
          uri: data.video.uri,
          name: data.video.name,
          type: 'video',
          link: data.video.link,
          picture_link: data.video.pictures.sizes.find((p) => p.width > 640)
            ?.link,
          status: data.video.status,
          is_playable: data.video.is_playable,
        };
        buffer.push(video);
      } else if (data.type == 'folder') {
        const folder: FolderType = {
          created_time: data.folder.created_time,
          modified_time: data.folder.modified_time,
          last_user_action_event_date: data.folder.last_user_action_event_date,
          uri: data.folder.uri,
          name: data.folder.name,
          type: 'folder',
        };
        buffer.push(folder);
      } else {
        console.warn('unknown type', data.type);
      }
    }
    return buffer;
  }
}
