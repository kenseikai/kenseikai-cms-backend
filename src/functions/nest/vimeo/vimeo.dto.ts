import { ApiExtraModels, ApiProperty, getSchemaPath } from '@nestjs/swagger';

export class FolderType {
  @ApiProperty()
  created_time: string;
  @ApiProperty()
  modified_time: string;
  @ApiProperty()
  last_user_action_event_date: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  uri: string;

  @ApiProperty()
  type: 'folder';
}

export class VideoType {
  @ApiProperty()
  created_time: string;
  @ApiProperty()
  modified_time: string;
  @ApiProperty()
  release_time: string;

  @ApiProperty()
  uri: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  type: 'video';
  @ApiProperty()
  link: string;

  @ApiProperty()
  picture_link: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  is_playable: boolean;
}

@ApiExtraModels(FolderType, VideoType)
export class VimeoRootFolderResponse {
  @ApiProperty({
    type: 'array',
    items: {
      oneOf: [
        { $ref: getSchemaPath(FolderType) },
        { $ref: getSchemaPath(VideoType) },
      ],
    },
  })
  items: (FolderType | VideoType)[];
}
