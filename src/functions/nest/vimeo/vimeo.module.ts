import { CacheModule, Module } from '@nestjs/common';
import { VimeoController } from './vimeo.controller';
import { VimeoService } from './vimeo.service';

@Module({
  imports: [
    CacheModule.register({
      ttl: 120,
      max: 10000,
    }),
  ],
  controllers: [VimeoController],
  providers: [VimeoService],
})
export class VimeoModule {}
