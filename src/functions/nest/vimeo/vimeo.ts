export interface VimeoResult<SingleDataType> {
  total: number;
  page: number;
  per_page: number;
  paging: {
    next: string | null;
    previous: string | null;
    first: string;
    last: string;
  };
  data: Array<SingleDataType>;
}

export interface VimeoBasicData {
  created_time: string;
  modified_time: string;
  last_user_action_event_date: string;
}

export interface VimeoFolder extends VimeoBasicData {
  name: string;
  privacy: {
    view: string;
  };
  resource_key: string;
  uri: string;
  link: string | null;
  metadata: {
    connections: {
      ancestor_path?: {
        uri: string;
        name: string;
      }[];

      parent_folder?: {
        uri: string;
      };
    };
  };
}
