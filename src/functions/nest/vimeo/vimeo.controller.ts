import {
  CacheInterceptor,
  Controller,
  Get,
  Param,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBasicAuth, ApiOkResponse } from '@nestjs/swagger';
import { VimeoRootFolderResponse } from './vimeo.dto';
import { VimeoService } from './vimeo.service';

@Controller('vimeo')
@UseInterceptors(CacheInterceptor)
export class VimeoController {
  constructor(private readonly vimeoService: VimeoService) {}
  @Get('folders')
  @ApiBasicAuth()
  async getFolders() {
    return this.vimeoService.getFolders();
  }

  @Get('root_folder')
  @ApiBasicAuth()
  @ApiOkResponse({
    type: () => VimeoRootFolderResponse,
  })
  async getRootFolder() {
    return {
      items: await this.vimeoService.getRootFolder(),
    };
  }

  @Get('folder/:folder_id')
  @ApiBasicAuth()
  @ApiOkResponse({
    type: () => VimeoRootFolderResponse,
  })
  async getFolder(@Param('folder_id') folder_id: string) {
    return {
      items: await this.vimeoService.getFolder(folder_id),
    };
  }

  // @Get('folders/:folder_id?')
  // async getFolder(@Param('folder_id') folder_id: string) {
  //   this.getFolder()
  // }
}
