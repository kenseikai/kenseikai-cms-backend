import { handlerPath } from '@libs/handlerResolver';
import type { AWS } from '@serverless/typescript';

export const nest: AWS['functions'][''] = {
  handler: `${handlerPath(__dirname)}/handler.main`,
  events: [
    {
      http: {
        method: 'any',
        path: '/',
        authorizer: {
          name: 'basicAuth',
          resultTtlInSeconds: 0,
          identitySource: 'method.request.header.Authorization',
          type: 'request',
        },
        cors: true,
      },
    },
    {
      http: {
        method: 'any',
        path: '/{proxy+}',
        authorizer: {
          name: 'basicAuth',
          resultTtlInSeconds: 0,
          identitySource: 'method.request.header.Authorization',
          type: 'request',
        },
        cors: true,
      },
    },
  ],
};
