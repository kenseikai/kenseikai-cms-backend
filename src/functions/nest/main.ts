import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { writeFileSync } from 'fs';
import { dump } from 'js-yaml';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  if (process.env.NODE_ENV !== 'production') {
    const config = new DocumentBuilder()
      .setTitle('Kenseikai CMS API')
      .setDescription('The Kenseikai CMS API description')
      .setVersion('1.0')
      .addTag('kenseikai')
      .addBearerAuth()
      .addBasicAuth()
      .addSecurity('basic', {
        type: 'http',
        scheme: 'basic',
      })
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);

    writeFileSync('swagger.yml', dump(document, {}));
  }

  await app.listen(process.env.PORT ?? 8000);
}
bootstrap();
