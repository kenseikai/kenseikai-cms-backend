import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import serverlessExpress from '@vendia/serverless-express';
import { Context, Handler } from 'aws-lambda';
import express from 'express';

import { AppModule } from './app.module';
import { cwd } from 'process';
import { STS } from 'aws-sdk';

let cachedServer: Handler;

async function bootstrap(context: Context) {
  if (!cachedServer) {
    console.log({ cwd, dirname: __dirname });
    const expressApp = express();
    const nestApp = await NestFactory.create(
      AppModule,
      new ExpressAdapter(expressApp),
    );

    nestApp.enableCors({
      credentials: true,
      origin: (requestOrigin, callback) => {
        callback(null, requestOrigin);
      },
    });

    await nestApp.init();

    cachedServer = serverlessExpress({ app: expressApp });

    if (!process.env.AWS_ACCOUNT_ID) {
      if (context.invokedFunctionArn) {
        process.env.AWS_ACCOUNT_ID = context.invokedFunctionArn.split(':')[4];
      } else {
        const sts = new STS();
        process.env.AWS_ACCOUNT_ID = (
          await sts.getCallerIdentity({}).promise()
        ).Account;
      }
    }
  }

  return cachedServer;
}

const nestHandler = async (event: any, context: Context, callback: any) => {
  const server = await bootstrap(context);
  return server(event, context, callback);
};

export const main = nestHandler;
